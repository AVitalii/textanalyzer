﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace TextAnalyzer.Helpers
{
    public class WordFrequencyCounter
    {
        public Dictionary<string, int> WordDictionary { get; private set; }

        public WordFrequencyCounter(bool caseSensetive = false)
        {
            WordDictionary = caseSensetive ?
                new Dictionary<string, int>() : 
                new Dictionary<string, int>(StringComparer.CurrentCultureIgnoreCase);
        }

        public Task CountAsync(string content)
        {
            if (string.IsNullOrWhiteSpace(content))
            {
                if (content == null)
                {
                    throw new ArgumentNullException($"{nameof(content) } is null");
                }
                return Task.FromResult(0);
            }

            var wordPattern = new Regex(@"\w+");
            
            return Task.Factory.StartNew(() =>
            {
                foreach(Match match in wordPattern.Matches(content))
                {
                    WordDictionary.TryGetValue(match.Value, out int currentCount);
                    currentCount++;
                    WordDictionary[match.Value] = currentCount;
                }
            });
        }
    }
}
