﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextAnalyzer.Services
{
    public class FileSystemDataService : IDataService
    {
        public int MaxScanDepth { get; set; } = 10;
        public List<string> SupportedExtensions { get; set; } = new List<string> { ".txt" };
        public async Task<string> GetItemContent(string source) => await File.ReadAllTextAsync(source);

        public void CollectContainerItems(string sourcePath, List<string> files, int level)
        {
            if ( (files == null) || (level > MaxScanDepth) || !Directory.Exists(sourcePath))
            {
                return;
            }

            foreach(var dirPath in Directory.GetDirectories(sourcePath))
            {
                CollectContainerItems(dirPath, files, level + 1);
            }
            
            var folderFiles = Directory.GetFiles(sourcePath)?
                .Where( p => !string.IsNullOrWhiteSpace(Path.GetExtension(p)) && SupportedExtensions.Contains(Path.GetExtension(p)))?.ToArray();
            files.AddRange(folderFiles);
        }

        public Task<bool> IsValid(string source)
        {
            return Task.FromResult(Directory.Exists(source));
        }

        public async Task Report(string output, Dictionary<string, int> dictionary)
        {
            var result = new StringBuilder();
            var strings = dictionary.OrderByDescending(it => it.Value).Select(it => $"{it.Key},{it.Value}").ToArray();
            await File.WriteAllLinesAsync(output, strings);
        }
    }
}
