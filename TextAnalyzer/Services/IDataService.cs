﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace TextAnalyzer.Services
{
    public interface IDataService
    {
        List<string> SupportedExtensions { get; set; }

        Task<string> GetItemContent(string source);

        void CollectContainerItems(string sourcePath, List<string> files, int level);

        Task Report(string output, Dictionary<string, int> dictionary);

        Task<bool> IsValid(string source);
    }
}
