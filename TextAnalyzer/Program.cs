﻿using Microsoft.Extensions.CommandLineUtils;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TextAnalyzer.Helpers;
using TextAnalyzer.Services;

namespace TextAnalyzer
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Console.WriteLine("Starting");

            var serviceProvider = new ServiceCollection()
                .AddSingleton<IDataService, FileSystemDataService>()
                .BuildServiceProvider();

            var cmd = new CommandLineApplication(throwOnUnexpectedArg: false);

            var argFolderPath = cmd.Option("-p | --path <value>", "Input folder path", CommandOptionType.SingleValue);
            var argOutput = cmd.Option("-o | --output <value>", "Output file path", CommandOptionType.SingleValue);

            cmd.OnExecute(async () =>
            {
                var dataService = serviceProvider.GetService<IDataService>();

                if (!await dataService.IsValid(argFolderPath.Value()))
                {
                    Console.WriteLine("The path paremeter is invalid");
                    return -1;
                }
                var wfc = new WordFrequencyCounter();

                try
                {
                    var files = new List<string>();
                    dataService.CollectContainerItems(argFolderPath.Value(), files, 0);
                    foreach(var file in files)
                    {
                        var content = await dataService.GetItemContent(file);
                        await wfc.CountAsync(content);
                    }

                    await dataService.Report(argOutput.Value(), wfc.WordDictionary);
                    Console.WriteLine("Finished");
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"For demo only ! Exception found { ex.Message }");
                    return -1;
                }
                return 0;
            });

            cmd.HelpOption("-? | -h | --help");
            cmd.Execute(args);
        }
    }
}
