﻿using System;
using System.Threading.Tasks;
using TextAnalyzer.Helpers;
using Xunit;

namespace TextAnalyzer.Tests.Helpers
{
    public class WordFrequencyCounterTest_Count
    {
        [Fact]
        public async Task Count_For_ValidData_ShouldReturn_ExpectedResult()
        {
            // Assign            
            var hiPart = "Hi";
            var hiCount = 1;
            var iPart = "I";
            var iCount = 2;

            var content = $"{hiPart},{iPart}'m an unit test and {iPart.ToLower()} passed";
            var totalWords = 8;
            var wfc = new WordFrequencyCounter();

            // Act

            await wfc.CountAsync(content);

            // Assert

            Assert.True(wfc.WordDictionary.ContainsKey(hiPart));
            wfc.WordDictionary.TryGetValue(hiPart, out int count);
            Assert.Equal(hiCount, count);

            Assert.True(wfc.WordDictionary.ContainsKey(iPart));
            wfc.WordDictionary.TryGetValue(iPart, out count);
            Assert.Equal(iCount, count);

            Assert.Equal(totalWords, wfc.WordDictionary.Count);
        }

        [Fact]
        public async Task Count_For_EmptyData_ShouldReturn_EmptyResult()
        {
            // Assign
            var content = string.Empty;
            var wfc = new WordFrequencyCounter();

            // Act
            await wfc.CountAsync(content);

            // Assert
            Assert.Empty(wfc.WordDictionary);
        }

        [Fact]
        public async Task Count_For_Null_ShouldThrow_ArgumentNullException()
        {
            // Assign
            string content = null;
            var wfc = new WordFrequencyCounter();

            // Act
            var exception = await Assert.ThrowsAsync<ArgumentNullException>(async () => await wfc.CountAsync(content));

            // Assert
            Assert.Contains("content is null", exception.Message);
        }
    }
}
